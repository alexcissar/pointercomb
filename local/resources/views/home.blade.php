@extends('user_app')
@section('content')
<div class="container">
    <div class="header">
        <div class="top"> <a href="http://www.geidai.ac.jp/labs/koizumi/index.html"> <img src="http://www.geidai.ac.jp/labs/koizumi/images/logo-02.png" alt="小泉文夫記念資料室" class="SCREEN" width="75%" height="75%">
            <img src="http://www.geidai.ac.jp/labs/koizumi/images/logo-03.png" alt="小泉文夫記念資料室" class="PRINT" width="75%" height="75%">
            </a> </div>
        <div class="rHeader">
            <ul class="rmenu"><li> <a href="http://www.geidai.ac.jp/labs/koizumi/contribution.html">寄付について</a> </li><li> <a href="contact.html">お問い合わせ</a> </li>
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/english/">English</a> </li>
                <li id="textsizer"> 文字サイズの変更
                    <ul class="textresizer">
                        <li> <a href="#" id="fontS"><img src="http://www.geidai.ac.jp/labs/koizumi/images/btn_font.png" width="12px" height="12px" alt="小"></a> </li>
                        <li> <a href="#" id="fontM"><img src="http://www.geidai.ac.jp/labs/koizumi/images/btn_font.png" width="12px" height="12px" alt="中"></a> </li>
                        <li> <a href="#" id="fontL"><img src="http://www.geidai.ac.jp/labs/koizumi/images/btn_font.png" width="12px" height="12px" alt="大"></a> </li>
                    </ul>
                </li>
            </ul>
                <ul class="rmenu">
          <li class="rink"><a href="http://koizumi2.ms.geidai.ac.jp/asia/jp/index.html">アジアの楽器図鑑</a></li>
          <li class="rink"><a href="http://www.geidai.ac.jp/labs/koizumi/edotokyodb.html">江戸東京音楽芸能DB</a></li>
        </ul>
      </div>
        </div>
    </div>
</div>
<!-- /container -->
<div class="menu">
    <div class="container">
        <ul class="gnavi SCREEN">
            <li> <a class="menu1" href="http://www.geidai.ac.jp/labs/koizumi/about.html"></a> </li>
            <li> <a class="menu2" href="http://www.geidai.ac.jp/labs/koizumi/collections.html"></a> </li>
            <li> <a class="menu3" href="http://www.geidai.ac.jp/labs/koizumi/guide.html"></a> </li>
            <li> <a class="menu4" href="http://www.geidai.ac.jp/labs/koizumi/award.html"></a> </li>
            <li> <a class="menu5" href="http://www.geidai.ac.jp/labs/koizumi/results.html"></a> </li>
        </ul>
        <img src="http://www.geidai.ac.jp/labs/koizumi/images/gnavi-p.png" height="35" width="940" class="PRINT">
    </div>
</div>
        <div class="main_2nd">
        <div class="line-box-shadow"></div>
        <div class="container">
        <div class="container2">
        <div class="bradcrumb"> <a href="http://www.geidai.ac.jp/labs/koizumi/index.html">ホーム</a> > <span class="current">江戸東京音楽芸術DB</span> </div>
        <img class="article_logo" src="http://www.geidai.ac.jp/labs/koizumi/images/title-list13.png">

        <div class="article940"><div id="map">
      <div class="map_control_div">
        <div style="width: 59px; height: 59px; overflow: hidden; position: absolute;">
          <img src="{{asset('image/map_tile/map_control.png')}}" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 59px; height: 492px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;">

          <div class="left_transport" style="position: absolute; left: 0px; top: 20px; width: 19.6666666666667px; height: 19.6666666666667px; cursor: pointer;"></div>
          <div class="right_transport" style="position: absolute; left: 39px; top: 20px; width: 19.6666666666667px; height: 19.6666666666667px; cursor: pointer;"></div>
          <div class="up_transport" style="position: absolute; left: 20px; top: 0px; width: 19.6666666666667px; height: 19.6666666666667px; cursor: pointer;"></div>
          <div class="down_transport" style="position: absolute; left: 20px; top: 39px; width: 19.6666666666667px; height: 19.6666666666667px; cursor: pointer;"></div>
        </div>

        <div id="zoom_slider_control"></div>
      </div></div>
    </div>

    <div class="operation">
      <div class="float_left">
        <button type="button"  class="btn btn-primary edo_age_radio" data-toggle="button">江戸時代</button>
        <button type="button"  class="btn btn-primary meichi_age_radio" data-toggle="button">明治時代</button>
        <button type="button"  class="btn btn-primary xianzai_age_radio active" data-toggle="button">現代</button>
      </div>

      <div class="float_right" style="margin-right: 11em; font-size: 12px">
          <div id="Slider" style="width: 120px"></div>
      </div>

      <div class="spacer"></div>
    </div>

    <div id="search_loading_status">
      <img src="{{asset('image/pointer/ajax-loader.gif')}}" width="100px" height="100px">
      <p>Now Loading…</p>
    </div>

    <div id="popup" class="ol-popup">
      <div class="profile-user-info" id="popup-content">
        <h2 class="pointer_name"></h2><br>
        <small class="pointer_kananame"></small>

        <div class="txt_right pointer_period"></div>

        <dl>
          <dt>当時の住所</dt>
          <dd class="pointer_address"></dd>
          <dt>現在の住所</dt>
          <dd class="pointer_curaddress"></dd>
        </dl>

        <p class="pointer_content"></p>

        <div class="pointer_images_div">
          <button class="btn btn-info pointer_image_href">写真を見る</button>
        </div>
      </div>
        </div>

        </div>		<div class="pagetop"> <a href="#pagetop">▲ページTOP</a> </div>
        </div>
        </div>
<div class="box-shadow"></div>
</div>
<div class="footer2">
    <div class="footernavi">
        <div class="fnavi">
            <ul class="footer_home">
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/index.html">ホーム</a> </li>
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/contribution.html">寄付について</a> </li>
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/sitemap.html">サイトマップ</a> </li>
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/english/">English</a> </li>
            </ul>
        </div>
        <div class="fnavi">
            <ul class="footer_about">
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/about.html">小泉文夫記念資料室について</a> </li>

                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/chronological.html">小泉文夫年譜</a> </li>
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/publications_1.html">小泉文夫著作集</a> </li>
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/room.html">室内概観図</a> </li>
                <li> <a href="http://www.wakadesignroom.com/VR/KOIZUMI_TEST/tour.html">全景パノラマ</a> </li>
            </ul>
        </div>
        <div class="fnavi">
            <ul class="footer_data">
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/collections.html">所蔵資料</a> </li>
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/collections_list.html">楽器</a> </li>
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/bcat.html">書籍</a> </li>
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/magazine.html">雑誌</a> </li>
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/coarsedata.html">雑資料</a> </li>
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/opentape.html">音響</a> </li>
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/dvd.html">映像</a> </li>
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/photo.html">写真</a> </li>
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/costume.html">民族衣装</a> </li>
            </ul>
        </div>
        <div class="fnavi">
            <ul class="footer_guide">
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/guide.html">利用案内</a> </li>
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/guide.html">資料室の利用について</a> </li>
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/regulations.html">所蔵資料の利用規定</a> </li>
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/contact.html">お問い合わせ</a> </li>
            </ul>
        </div>
        <div class="fnavi">
            <ul class="footer_award">
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/award.html">小泉文夫音楽賞</a> </li>
            </ul>
        </div>
        <div class="fnavi">
            <ul class="footer_research">
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/results.html">研究成果</a> </li>
                <li> <a href="http://koizumi2.ms.geidai.ac.jp/asia/jp/index.html">アジアの楽器図鑑</a> </li>
                <li> <a href="http://www.geidai.ac.jp/labs/koizumi/edotokyodb.html">江戸東京音楽芸能 DB</a> </li>
            </ul>
        </div>
        <ul class="copyright">
            <li> c 2014 Koizumi Fumio Memorial Archives.  All rights reserved. </li>
            <li> <a href="http://www.geidai.ac.jp/labs/koizumi/about_this_site.html">サイトのご利用について</a> </li>
        </ul>
        <div class="address">〒110-8714　東京都台東区上野公園12-8　東京藝術大学音楽学部　小泉文夫記念資料室</div>
    </div>
</div>

<div id="pointer_image_album"></div>
@endsection
