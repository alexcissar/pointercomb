<!DOCTYPE html>
<html lang="ja" style="overflow: scroll; font-size: 100%">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>江戸東京音楽芸術DB-小泉文夫記念資料室</title>
	<link href="{{asset('css/user_mapview/ol.css')}}" rel="stylesheet">
	<link href="{{asset('css/ace/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/ace/font-awesome.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/ace/bootstrap-responsive.min.css') }}" rel="stylesheet">
	<link href="{{asset('css/ace/ace.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/ace/ace-rtl.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/ace/ace-skins.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/ace/jquery-ui-1.10.3.custom.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/user_mapview/mapview.css')}}" rel="stylesheet">
	<link href="{{asset('css/user_mapview/jquery.fancybox.css')}}" rel="stylesheet">
	<link href="{{asset('css/user_mapview/jquery.fancybox-buttons.css')}}" rel="stylesheet">
	<link href="{{asset('css/user_mapview/popup.css')}}" rel="stylesheet">
	<link href="//www.geidai.ac.jp/labs/koizumi/css/style3.css" rel="stylesheet">
	<link href="//www.geidai.ac.jp/labs/koizumi/css/print.css" rel="stylesheet" media="print">
	<script src="{{asset('js/ace/ace-extra.min.js')}}"></script>
	<script src="{{asset('js/ace/jquery.min.js')}}"></script>
	<script src="{{asset('js/ace/bootstrap.min.js')}}"></script>
	<script src="{{asset('js/ace/typeahead-bs2.min.js')}}"></script>
	<script src="{{asset('js/ace/jquery-ui-1.10.3.custom.min.js')}}"></script>
	<script src="{{asset('js/ace/jquery.ui.touch-punch.min.js')}}"></script>
	<script src="{{asset('js/ace/jquery.slimscroll.min.js')}}"></script>
	<script src="{{asset('js/ace/jquery.easy-pie-chart.min.js')}}"></script>
	<script src="{{asset('js/ace/jquery.sparkline.min.js')}}"></script>
	<script src="{{asset('js/ace/ace-elements.min.js')}}"></script>
	<script src="{{asset('js/ace/ace.min.js')}}"></script>
	<script src="{{asset('js/user_mapview/jquery.fancybox.js')}}"></script>
	<script src="{{asset('js/user_mapview/jquery.fancybox-buttons.js')}}"></script>
	<script src="{{asset('js/openlayer/ol.js')}}"></script>
	<script src="{{asset('js/user_mapview/mapinit.js')}}"></script>
	<script src="{{asset('js/user_mapview/pointer-manage.js')}}"></script>
	<script src="{{asset('js/user_mapview/mapview.js')}}"></script>
	<script src="//www.geidai.ac.jp/labs/koizumi/js/jquery.cookie.js"></script>
	<script src="//www.geidai.ac.jp/labs/koizumi/js/jquery.textresizer.min.js"></script>
	<!-- pagetopの設定 -->
	<script type="text/javascript">
		$(function() {
			//ページ上部へ戻る
			$(".pagetop").click(function () {
				$('html,body').animate({ scrollTop: 0 }, 'fast');
				return false;
			});
		});
	</script>
	<!-- アコーディオンメニューの設定 -->
	<script type="text/javascript">
		$(function(){
			$("ul.collection").hide();
			$("li#show").click(function(){
				$(this).next().slideToggle();
			});
		})
	</script>
	<!-- 文字サイズ変更の設定 -->
	<script type="text/javascript">
		jQuery(document).ready( function() {
			jQuery( "#textsizer a" ).textresizer({
				target: ".article",                       // 対象要素
				type: "fontSize",                        // サイズ指定方法
				sizes: [ "0.9em", "1em", "1.09em"],// フォントサイズ
				selectedIndex: 1                         // 初期表示
			});
		});
	</script>

</head>
<body>
	@yield('content')
</body>
</html>
